<?php
declare(strict_types=1);

use mef\Pagination\Paginator;

require_once __DIR__ . '/../vendor/autoload.php';

const PAGE_SIZE = 2;

$paginator = new Paginator(PAGE_SIZE);

$pdo = new PDO('sqlite::memory:');
$db = new mef\Db\Driver\PdoDriver($pdo);
$writer = new mef\Sql\Writer\SqliteWriter($db);
$db = new mef\Sql\Driver\SqlDriver($writer);

// Set up table

$db->execute('CREATE TABLE person (
	id INTEGER,
	name TEXT,
	age INTEGER
)');

$st = $db->prepare('INSERT INTO person VALUES (:id, :name, :age)');
$st->bindParameters([
	':id' => &$id,
	':name' => &$name,
	':age' => &$age
]);

// insert data

$data = [
	[1, 'John Doe', 42],
	[2, 'Jane Doe', null],
	[3, 'George', 23],
	[4, 'Paul', 89],
	[5, 'Sam', 2],
];

foreach($data as list($id, $name, $age))
{
	$st->execute();
}

// paginate by page. From youngest to oldest.
for ($pageNumber = 0; $pageNumber === 0 || $paginatedQuery->containsLastRecord() === false; ++$pageNumber)
{
	$pagination = $paginator->page(['age'], $pageNumber);

	$query = $db->select()->from('person');

	$paginatedQuery = $pagination->paginate($query);

	echo 'Page ' . $pageNumber . PHP_EOL;
	echo $paginatedQuery, PHP_EOL;
	print_r($paginatedQuery->query()->fetchAll());
	print_r($paginatedQuery->getStats());
}


// paginate by reference. From oldest to youngest.

echo 'Paginated by reference.', PHP_EOL;

// Use a fake reference for the first query, using a large age to make sure it
// is the first record.
$reference = $db->query('SELECT * FROM person ORDER BY age DESC LIMIT 1')->fetchRow();

while (true)
{
	$pagination = $paginator->after(
		$reference,    // The object to sort after.
		['age', 'id'], // Sort by fields. The last one should always be a unique field.
		0,             // Use default page size.
		true           // Invert order (DESC).
	);

	$query = $db->select()->from('person');

	$paginatedQuery = $pagination->paginate($query);

	$results = $paginatedQuery->query()->fetchAll();

	echo $paginatedQuery, PHP_EOL;
	print_r($results);
	print_r($paginatedQuery->getStats());

	if ($paginatedQuery->containsLastRecord())
	{
		break;
	}

	$reference = array_pop($results);
}
