<?php
declare(strict_types=1);
namespace mef\Pagination\Type;

use InvalidArgumentException;
use mef\Pagination\Sql\PaginatedQueryInterface;
use mef\Pagination\Sql\RelativePaginatedQuery;
use mef\Sql\Builder\SelectBuilder;

/**
 * A class that defines how to paginate a select query.
 */
class RelativePagination extends AbstractPagination
{
	/**
	 * @var bool    true if the record set needs to be reversed after fetching
	 */
	private $isReversed;

	/**
	 * Constructor
	 *
	 * @param array $fields     an array of PaginationField objects
	 * @param int   $pageSize   the maximum number of rows to return
	 * @param bool  $isReversed return true if the comparison is descending
	 */
	public function __construct(array $fields, int $pageSize, bool $isReversed = false)
	{
		parent::__construct($fields, $pageSize);
		$this->isReversed = $isReversed;
	}

	/**
	 * Return true if the results should be reversed (descending page order).
	 *
	 * @return bool
	 */
	public function isReversed() : bool
	{
		return $this->isReversed;
	}

	/**
	 * Build a paginated query based on the supplied query.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder
	 *
	 * @return \mef\PaginatedQueryInterface
	 */
	public function paginate(SelectBuilder $query) : PaginatedQueryInterface
	{
		return new RelativePaginatedQuery($query, $this);
	}
}

