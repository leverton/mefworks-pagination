<?php
declare(strict_types=1);
namespace mef\Pagination\Type;

use InvalidArgumentException;

/**
 * Defines how a single field should be ordered.
 */
class PaginationField
{
	const ASC = 'ASC';
	const DESC = 'DESC';

	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var ?string
	 */
	private $value;

	/**
	 * @var string
	 */
	private $order;

	/**
	 * Constructor
	 *
	 * @param string $name   name of the field
	 * @param ?string $value  value of the field
	 * @param string $order  PaginationField::ASC or PaginationField::DESC
	 */
	public function __construct(string $name, ?string $value, string $order = self::ASC)
	{
		if ($order !== self::ASC && $order !== self::DESC)
		{
			throw new InvalidArgumentException('$order must either be PaginationField::ASC or PaginationField::DESC');
		}

		$name = trim($name);

		if ($name === '')
		{
			throw new InvalidArgumentException('The name for the PaginationField is empty.');
		}

		$this->name = $name;
		$this->value = $value;
		$this->order = $order;
	}

	/**
	 * Return the name of the field.
	 *
	 * @return string
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * Return the value of the field.
	 *
	 * @return ?string
	 */
	public function getValue() : ?string
	{
		return $this->value;
	}

	/**
	 * Return true if the field comparison is in ascending order.
	 * Return false if it is in descending order.
	 *
	 * @return bool
	 */
	public function isAscending() : bool
	{
		return $this->order === self::ASC;
	}
}