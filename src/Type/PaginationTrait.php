<?php
declare(strict_types = 1);
namespace mef\Pagination\Type;

use RuntimeException;

/**
 * A trait for commands that handle pagination.
 *
 * It is assumed that the pagination parameter is in the $options array with
 * the name of 'pagination'.
 */
trait PaginationTrait
{
	/**
	 * @var array
	 */
	protected $options;

	/**
	 * Return true if the command has a pagination object.
	 *
	 * @return boolean
	 */
	public function hasPagination() : bool
	{
		return isset($this->options['pagination']) === true &&
			($this->options['pagination'] instanceof PaginationInterface) === true;
	}

	/**
	 * Return true if the command has declared that it wants paginated stats
	 * returned.
	 *
	 * @return bool
	 */
	public function wantsPaginatedStats() : bool
	{
		return $this->hasPagination() === true &&
			isset($this->options['pagination_stats']) === true &&
			$this->options['pagination_stats'] === true;
	}

	/**
	 * Return the pagination object.
	 *
	 * If it does not exist, an exception will be thrown.
	 *
	 * @return \mef\Pagination\Type\PaginationInterface
	 * @throws \RuntimeException
	 */
	public function getPagination() : PaginationInterface
	{
		if (!$this->hasPagination())
		{
			throw new RuntimeException('No pagination exists for this command.');
		}

		return $this->options['pagination'];
	}
}