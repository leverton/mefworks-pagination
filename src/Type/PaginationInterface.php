<?php
declare(strict_types=1);
namespace mef\Pagination\Type;

use InvalidArgumentException;
use mef\Pagination\Sql\PaginatedQueryInterface;
use mef\Sql\Builder\SelectBuilder;

/**
 * An interface describing basic pagination parameters.
 */
interface PaginationInterface
{
	/**
	 * Return the field objects.
	 *
	 * @return array
	 */
	public function getFields() : array;

	/**
	 * Return the maximum number of rows.
	 *
	 * @return int
	 */
	public function getPageSize() : int;

	/**
	 * Build a paginated query based on the supplied query.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder
	 *
	 * @return \mef\PaginatedQueryInterface
	 */
	public function paginate(SelectBuilder $query) : PaginatedQueryInterface;

	/**
	 * Return a pagination object with fields renamed according to the
	 * supplied aliases.
	 *
	 * @param  array  $fieldsAliases  An array of alias as key/value pairs.
	 *                                  ['oldName' => 'newName']
	 *
	 * @return \mef\Pagination\Type\PaginationInterface
	 */
	public function mapFieldNames(array $fieldsAliases) : PaginationInterface;
}

