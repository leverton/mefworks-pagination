<?php
declare(strict_types=1);
namespace mef\Pagination\Type;

use InvalidArgumentException;

/**
 * A class that defines how to paginate a select query.
 */
abstract class AbstractPagination implements PaginationInterface
{
	/**
	 * @var array   an array of PaginationField objects
	 */
	private $fields;

	/**
	 * @var int     the maximum number of rows to return
	 */
	private $pageSize;

	/**
	 * Constructor
	 *
	 * @param array $fields     an array of PaginationField objects
	 * @param int   $pageSize   the maximum number of rows to return
	 */
	public function __construct(array $fields, int $pageSize)
	{
		foreach ($fields as $field)
		{
			if (($field instanceof PaginationField) === false)
			{
				throw new InvalidArgumentException('$fields must be an array of ' . PaginationField::class . ' objects');
			}
		}

		if ($pageSize <= 0)
		{
			throw new InvalidArgumentException('$pageSize must be greater than 0');
		}

		$this->fields = $fields;
		$this->pageSize = $pageSize;
	}

	/**
	 * Return the field objects.
	 *
	 * @return array
	 */
	public function getFields() : array
	{
		return $this->fields;
	}

	/**
	 * Return the maximum number of rows.
	 *
	 * @return int
	 */
	public function getPageSize() : int
	{
		return $this->pageSize;
	}

	/**
	 * Return a pagination object with fields renamed according to the
	 * supplied aliases.
	 *
	 * @param  array  $fieldsAliases  An array of alias as key/value pairs.
	 *                                  ['oldName' => 'newName']
	 *
	 * @return \mef\Pagination\Type\PaginationInterface
	 */
	public function mapFieldNames(array $fieldsAliases) : PaginationInterface
	{
		if ($fieldsAliases === [] || $this->fields === [])
		{
			return $this;
		}

		$pagination = clone $this;

		$pagination->fields = array_map(function (PaginationField $field) use ($fieldsAliases)
		{
			if (isset($fieldsAliases[$field->getName()]) === true)
			{
				return new PaginationField(
					$fieldsAliases[$field->getName()],
					$field->getValue(),
					$field->isAscending() ? PaginationField::ASC : PaginationField::DESC
				);
			}
			else
			{
				return $field;
			}
		}, $pagination->fields);

		return $pagination;
	}
}

