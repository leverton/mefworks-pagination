<?php
declare(strict_types = 1);

namespace mef\Pagination\Type;

interface PaginatedCommandInterface
{
	/**
	 * Return true if the command has a pagination object.
	 *
	 * @return boolean
	 */
	public function hasPagination() : bool;

	/**
	 * Return true if the command has declared that it wants paginated stats
	 * returned.
	 *
	 * @return bool
	 */
	public function wantsPaginatedStats() : bool;

	/**
	 * Return the pagination object.
	 *
	 * If it does not exist, an exception will be thrown.
	 *
	 * @return \mef\Pagination\Type\PaginationInterface
	 * @throws \RuntimeException
	 */
	public function getPagination() : PaginationInterface;
}