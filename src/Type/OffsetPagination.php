<?php
declare(strict_types=1);
namespace mef\Pagination\Type;

use InvalidArgumentException;
use mef\Pagination\Sql\PaginatedQueryInterface;
use mef\Pagination\Sql\OffsetPaginatedQuery;
use mef\Sql\Builder\SelectBuilder;

/**
 * A class that defines how to paginate a select query.
 */
class OffsetPagination extends AbstractPagination
{
	/**
	 * @var int  The number of records to skip.
	 */
	private $offset;

	/**
	 * Constructor
	 *
	 * @param array $fields     an array of PaginationField objects
	 * @param int   $pageSize   the maximum number of rows to return
	 * @param int   $offset     the number of records to skip
	 */
	public function __construct(array $fields, int $pageSize, int $offset = 0)
	{
		parent::__construct($fields, $pageSize);
		$this->offset = $offset;
	}

	/**
	 * Return the number of records to skip.
	 *
	 * @return int
	 */
	public function getOffset() : int
	{
		return $this->offset;
	}

	/**
	 * Build a paginated query based on the supplied query.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder
	 *
	 * @return \mef\PaginatedQueryInterface
	 */
	public function paginate(SelectBuilder $query) : PaginatedQueryInterface
	{
		return new OffsetPaginatedQuery($query, $this);
	}
}

