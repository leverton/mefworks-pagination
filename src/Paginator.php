<?php
declare(strict_types=1);
namespace mef\Pagination;

use InvalidArgumentException;
use mef\Pagination\Type\{OffsetPagination, PaginationField, RelativePagination};

/**
 * A helper class to create a Pagination object.
 */
class Paginator
{
	const DEFAULT_PAGE_SIZE = 100;

	/**
	 * @var int  The default page size is set by the constructor. It will be
	 *           used when no page size is specified with before() or after().
	 */
	private $defaultPageSize;

	/**
	 * Constructor
	 *
	 * @param $defaultPageSize  The default page size to use for pagination.
	 */
	public function __construct(int $defaultPageSize = self::DEFAULT_PAGE_SIZE)
	{
		if ($defaultPageSize <= 0)
		{
			throw new InvalidArgumentException('$defaultPageSize must be greater than 0');
		}

		$this->defaultPageSize = $defaultPageSize;
	}

	/**
	 * Return a pagination object that represents the page before the supplied
	 * reference data.
	 *
	 * @param array  $reference     the key-value array containing the keys in $fields
	 * @param array  $fields        the fields to sort by
	 * @param int    $pageSize      the maximum number of records to return
	 * @param bool   $invertOrder   Set to true to invert the order of the $fields.
	 *
	 * @return \mef\Pagination\Type\RelativePagination
	 */
	public function before(array $reference, array $fields, int $pageSize = 0, bool $invertOrder = false) : RelativePagination
	{
		return $this->createPagination(
			$reference,
			$fields,
			true,
			$pageSize ?: $this->defaultPageSize,
			$invertOrder
		);
	}

	/**
	 * Return a pagination object that represents the page after the supplied
	 * reference data.
	 *
	 * @param array  $reference     the key-value array containing the keys in $fields
	 * @param array  $fields        the fields to sort by
	 * @param int    $pageSize      the maximum number of records to return
	 * @param bool   $invertOrder   Set to true to reverse the order of the $fields.
	 *
	 * @return \mef\Pagination\Type\RelativePagination
	 */
	public function after(array $reference, array $fields, int $pageSize = 0, bool $invertOrder = false) : RelativePagination
	{
		return $this->createPagination(
			$reference,
			$fields,
			false,
			$pageSize ?: $this->defaultPageSize,
			$invertOrder
		);
	}

	/**
	 * Return a pagination object that represents the Nth page.
	 *
	 * @param array $fields        the fields to sort by
	 * @param int   $pageNumber    the zero-based page number
	 * @param int   $pageSize      the number of records per page
	 * @param bool  $invertOrder   Set to true to invert the order of the $fields.
	 *                                (ASC becomes DESC; DESC becomes ASC)
	 *
	 * @return \mef\Pagination\Type\OffsetPagination
	 */
	public function page(array $fields, int $pageNumber, int $pageSize = 0, bool $invertOrder = false) : OffsetPagination
	{
		if ($pageNumber < 0)
		{
			throw new InvalidArgumentException('$pageNumber cannot be negative');
		}

		$pageSize = $pageSize ?: $this->defaultPageSize;

		$paginationFields = [];

		foreach ($fields as $field => $fieldOrder)
		{
			if (is_int($field) === true)
			{
				$field = $fieldOrder;
				$fieldOrder = PaginationField::ASC;
			}

			if ($invertOrder === true)
			{
				$fieldOrder = ($fieldOrder === PaginationField::ASC) ? PaginationField::DESC : PaginationField::ASC;
			}

			$paginationFields[] = new PaginationField($field, '', $fieldOrder);
		}

		return new OffsetPagination($paginationFields, $pageSize, $pageNumber * $pageSize);
	}

	/**
	 * Build and return a pagination object based on the parameters.
	 *
	 * @param array  $reference    the key-value array containing the keys in $fields
	 * @param array  $fields       the fields to sort by
	 * @param bool   $isReversed   true if the results should be reversed after fetched
	 * @param int    $pageSize     the maximum number of records to return
	 * @param bool   $invertOrder  Set to true to invert the order of the $fields.
	 *                               (ASC becomes DESC; DESC becomes ASC)
	 *
	 * @return \mef\Pagination\Type\RelativePagination
	 */
	private function createPagination(array $reference, array $fields, bool $isReversed, int $pageSize, bool $invertOrder) : RelativePagination
	{
		$paginationFields = [];
		$isReferenceIndexed = false;

		if (array_values($reference) === $reference)
		{
			$isReferenceIndexed = true;
			if (count($reference) !== count($fields))
			{
				throw new InvalidArgumentException('Number of $reference values does not equal number of $fields.');
			}
		}

		foreach ($fields as $field => $fieldOrder)
		{
			if (is_int($field) === true)
			{
				$field = $fieldOrder;
				$fieldOrder = PaginationField::ASC;
			}

			if ($isReferenceIndexed === true)
			{
				$referenceValue = array_shift($reference);
			}
			else
			{
				if (array_key_exists($field, $reference) === false)
				{
					throw new InvalidArgumentException('$reference did not contain ' . $field);
				}

				$referenceValue = $reference[$field];
			}

			if ($isReversed ^ $invertOrder === true)
			{
				$fieldOrder = ($fieldOrder === PaginationField::ASC) ? PaginationField::DESC : PaginationField::ASC;
			}

			if ($referenceValue !== null)
			{
				$referenceValue = (string) $referenceValue;
			}

			$paginationFields[] = new PaginationField($field, $referenceValue, $fieldOrder);
		}

		return new RelativePagination($paginationFields, $pageSize, $isReversed);
	}
}
