<?php
declare(strict_types=1);
namespace mef\Pagination\Sql;

use RuntimeException;
use mef\Db\RecordSet\ArrayRecordSet;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Pagination\Type\RelativePagination;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Expression;
use mef\Sql\WhereClause;

/**
 * A paginated query automatically adds pagination controls around an existing
 * query.
 */
class RelativePaginatedQuery extends AbstractPaginatedQuery
{
	/**
	 * @var bool
	 */
	private $moreAfter = false;

	/**
	 * @var int
	 */
	private $count = 0;

	/**
	 * @var \mef\Pagination\Type\RelativePagination
	 */
	private $pagination;

	/**
	 * @var bool
	 */
	private $didQuery = false;

	/**
	 * Constructor. Create a new SelectBuilder object that adheres to
	 * the pagination parameters.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder            $query
	 * @param \mef\Pagination\Type\RelativePagination   $pagination
	 *
	 */
	public function __construct(SelectBuilder $query, RelativePagination $pagination)
	{
		parent::__construct($query, $pagination);

		$clauses = new WhereClause;

		$fields = $pagination->getFields();
		$fieldCount = count($fields);

		for ($i = 0; $i < $fieldCount; ++$i)
		{
			$c = new WhereClause;

			if ($fields[$i]->getValue() === null)
			{
				if ($fields[$i]->isAscending() === false)
				{
					// Nothing can be before a NULL value (assumption NULLs sorted first),
					// so skip this specific condition altogether.
					continue;
				}

				$c->where($fields[$i]->getName(), '<>', null);
			}
			else
			{
				if ($fields[$i]->isAscending() === false)
				{
					$c->where((new WhereClause)->
						where($fields[$i]->getName(), '<', $fields[$i]->getValue())->
						orWhere($fields[$i]->getName(), null)
					);
				}
				else
				{
					$c->where($fields[$i]->getName(), '>', $fields[$i]->getValue());
				}
			}

			for ($j = 0; $j < $i; ++$j)
			{
				$c->where($fields[$j]->getName(), $fields[$j]->getValue());
			}

			$clauses->orWhere($c);
		}

		if ($clauses->getExpressions() === [])
		{
			// In some cases the reference may be entirely null, in which case there are no
			// records prior to it.
			$clauses->where(new Expression('FALSE'));
		}

		$this->where($clauses);

		foreach ($fields as $field)
		{
			$this->orderBy($field->getName(), $field->isAscending() ? 'ASC' : 'DESC');
		}

		$this->pagination = $pagination;
	}

	/**
	 * Reverses the query order if the sort order was DESC.
	 *
	 * @return \mef\Db\RecordSet\RecordSetInterface
	 */
	public function query() : RecordSetInterface
	{
		$results = iterator_to_array(parent::query());

		if (count($results) > $this->pagination->getPageSize())
		{
			unset($results[$this->pagination->getPageSize()]);
			$this->moreAfter = true;
		}

		$this->count = count($results);

		if ($this->pagination->isReversed() === true)
		{
			$results = array_reverse($results);
		}

		$this->didQuery = true;

		return new ArrayRecordSet($results);
	}

	/**
	 * Get the number of records remaining after the last record of the
	 * results.
	 *
	 * @return int
	 */
	public function getRemainingCount() : int
	{
		if ($this->didQuery === false)
		{
			throw new RuntimeException('query() must be called prior to calling getRemainingCount()');
		}

		return $this->count() - $this->count;
	}

	/**
	 * Return true if the resultset contains the first record.
	 *
	 * @return bool
	 */
	public function containsFirstRecord() : bool
	{
		if ($this->didQuery === false)
		{
			throw new RuntimeException('query() must be called prior to calling containsFirstRecord()');
		}

		if ($this->pagination->isReversed() === true)
		{
			return $this->moreAfter === false;
		}
		else
		{
			return $this->getOriginalQuery()->count() === $this->count();
		}
	}

	/**
	 * Return true if the resultset contains the last record.
	 *
	 * @return bool
	 */
	public function containsLastRecord() : bool
	{
		if ($this->didQuery === false)
		{
			throw new RuntimeException('query() must be called prior to calling containsLastRecord()');
		}

		if ($this->pagination->isReversed() === false)
		{
			return $this->moreAfter === false;
		}
		else
		{
			return $this->getOriginalQuery()->count() === $this->count();
		}
	}
}
