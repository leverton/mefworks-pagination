<?php
declare(strict_types=1);
namespace mef\Pagination\Sql;

use RuntimeException;
use mef\Db\RecordSet\ArrayRecordSet;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Pagination\Type\OffsetPagination;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\WhereClause;

/**
 * A paginated query automatically adds pagination controls around an existing
 * query.
 */
class OffsetPaginatedQuery extends AbstractPaginatedQuery
{
	/**
	 * @var bool
	 */
	private $moreAfter = false;

	/**
	 * @var int
	 */
	private $count = 0;

	/**
	 * @var \mef\Pagination\Type\OffsetPagination
	 */
	private $pagination;

	/**
	 * @var bool
	 */
	private $didQuery = false;

	/**
	 * Constructor. Create a new SelectBuilder object that adheres to
	 * the pagination parameters.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder         $query
	 * @param \mef\Pagination\Type\OffsetPagination  $pagination
	 *
	 */
	public function __construct(SelectBuilder $query, OffsetPagination $pagination)
	{
		parent::__construct($query, $pagination);

 		foreach ($pagination->getFields() as $field)
 		{
			$this->orderBy($field->getName(), $field->isAscending() ? 'ASC' : 'DESC');
		}

		$this->offset($pagination->getOffset());
		$this->pagination = $pagination;
	}

	/**
	 * Reverses the query order if the sort order was DESC.
	 *
	 * @return \mef\Db\RecordSet\RecordSetInterface
	 */
	public function query() : RecordSetInterface
	{
		$results = iterator_to_array(parent::query());

		if (count($results) > $this->pagination->getPageSize())
		{
			unset($results[$this->pagination->getPageSize()]);
			$this->moreAfter = true;
		}

		$this->count = count($results);
		$this->didQuery = true;

		return new ArrayRecordSet($results);
	}

	/**
	 * Get the number of records remaining after the last record of the
	 * results.
	 *
	 * @return int
	 */
	public function getRemainingCount() : int
	{	
		if ($this->didQuery === false)
		{
			throw new RuntimeException('query() must be called prior to calling getRemainingCount()');
		}

		return $this->count() - $this->count - $this->pagination->getOffset();
	}

	/**
	 * Return true if the resultset contains the first record.
	 *
	 * @return bool
	 */
	public function containsFirstRecord() : bool
	{
		return $this->pagination->getOffset() === 0;
	}

	/**
	 * Return true if the resultset contains the last record.
	 *
	 * @return bool
	 */
	public function containsLastRecord() : bool
	{
		if ($this->didQuery === false)
		{
			throw new RuntimeException('query() must be called prior to calling containsLastRecord()');
		}

		return $this->moreAfter === false;
	}
}
