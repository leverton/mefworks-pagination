<?php
declare(strict_types=1);
namespace mef\Pagination\Sql;

use mef\Db\RecordSet\ArrayRecordSet;
use mef\Db\RecordSet\RecordSetInterface;
use mef\Pagination\Type\PaginationInterface;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\WhereClause;

/**
 * A paginated query automatically adds pagination controls around an existing
 * query.
 */
abstract class AbstractPaginatedQuery extends SelectBuilder implements PaginatedQueryInterface
{
	/**
	 * @var \mef\Sql\Builder\SelectBuilder
	 */
	private $originalQuery;

	/**
	 * @var \mef\Pagination\Type\PaginationInterface
	 */
	private $pagination;

	/**
	 * Constructor. Create a new SelectBuilder object that adheres to
	 * the pagination parameters.
	 *
	 * @param \mef\Sql\Builder\SelectBuilder            $query
	 * @param \mef\Pagination\Type\PaginationInterface  $pagination
	 *
	 */
	public function __construct(SelectBuilder $query, PaginationInterface $pagination)
	{
		$this->originalQuery = $query;

		// copy primitives
		$this->fields = $query->fields;
		$this->limit = $query->limit;
		$this->fromTables = $query->fromTables;
		$this->joins = $query->joins;

		// clone complex objects
		$this->__whereClause = clone $query->__whereClause;
		$this->__orderBy = clone $query->__orderBy;
		$this->__groupBy = clone $query->__groupBy;
		$this->__having = clone $query->__having;

		$this->setWriter($query->getWriter());

		$this->limit($pagination->getPageSize() + 1);

		$this->pagination = $pagination;
	}

	public function getOriginalQuery() : SelectBuilder
	{
		return $this->originalQuery;
	}

	public function getPagination() : PaginationInterface
	{
		return $this->pagination;
	}

	public function getStats() : array
	{
		$pageSize = $this->pagination->getPageSize();
		$totalCount = $this->originalQuery->count();
		$remainingCount = $this->getRemainingCount();

		return [
			'pageSize' => $pageSize,
			'totalCount' => $totalCount,
			'remainingCount' => $remainingCount,
			'containsFirst' => $this->containsFirstRecord(),
			'containsLast' => $this->containsLastRecord(),
		];
	}
}
