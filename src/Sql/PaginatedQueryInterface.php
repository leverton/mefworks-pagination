<?php
declare(strict_types=1);
namespace mef\Pagination\Sql;

use mef\Pagination\Type\PaginationInterface;
use mef\Sql\Builder\SelectBuilder;

/**
 * An interface that defines a paginated query.
 */
interface PaginatedQueryInterface
{
	/**
	 * Reverses the query order if the sort order was DESC.
	 *
	 * @return \mef\Db\RecordSet\RecordSetInterface
	 */
	public function query();

	/**
	 * Get the number of records remaining after the last record of the
	 * results.
	 *
	 * @return int
	 */
	public function getRemainingCount() : int;

	/**
	 * Return true if the resultset contains the first record.
	 *
	 * @return bool
	 */
	public function containsFirstRecord() : bool;

	/**
	 * Return true if the resultset contains the last record.
	 *
	 * @return bool
	 */
	public function containsLastRecord() : bool;

	/**
	 * Return the original SelectBuilder that was used as the basis for
	 * pagination.
	 *
	 * @return \mef\Sql\Builder\SelectBuilder
	 */
	public function getOriginalQuery() : SelectBuilder;

	/**
	 * Return the pagination specifications.
	 *
	 * @return \mef\Pagination\Type\PaginationInterface
	 */
	public function getPagination() : PaginationInterface;

	/**
	 * Return the statistics about the resultset as an array.
	 *
	 * @return array
	 */
	public function getStats() : array;
}
