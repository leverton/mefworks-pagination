<?php
declare(strict_types=1);

use mef\Pagination\Paginator;

class PaginatorTest extends \PHPUnit\Framework\TestCase
{
	const DATA = [
		[1,  'Herman',    88],
		[2,  'Ahmad',     85],
		[3,  'Callum',    84],
		[4,  'Bradley',   84],
		[5,  'Antonio',   83],
		[6,  'Sebastian', 82],
		[7,  'William',   80],
		[8,  'Joe',       79],
		[9,  'Isaiah',    76],
		[10, 'Patrick',   75],
		[11, 'Martha',    75],
		[12, 'Alyssa',    74],
		[13, 'Jessica',   71],
		[14, 'Taylor',    70],
		[15, 'Ella',      68],
		[16, 'Courtney',  67],
		[17, 'Linda',     66],
		[18, 'Julie',     62],
		[19, 'Nadia',     62],
		[20, 'Karen',     53],
		[21, 'Alex',      52],
		[22, 'Richard',   50],
		[23, 'Howard',    48],
		[24, 'Erik',      47],
		[25, 'Yusuf',     46],
		[26, 'Melvin',    45],
		[27, 'Keaton',    36],
		[28, 'Floyd',     35],
		[29, 'Andrew',    34],
		[30, 'Mason',     32],
		[31, 'Ida',       32],
		[32, 'Layla',     31],
		[33, 'Carla',     31],
		[34, 'Michaela',  28],
		[35, 'Lauren',    25],
		[36, 'Harmony',   24],
		[37, 'Cassie',    18],
		[38, 'Angela',    17],
		[39, 'Victoria',  17],
		[40, 'Bethany',   15],
		[41, 'Nathan',    14],
		[42, 'Alan',      14],
		[43, 'Miles',     12],
		[44, 'James',     12],
		[45, 'Jim',        8],
		[46, 'Alice',      8],
		[47, 'Holly',      7],
		[48, 'Hazel',      6],
		[49, 'Lana',       6],
		[50, 'Alexandra',  5],
		[51, 'Caleb',      4],
		[52, 'Michael',    4],
		[53, 'Billy',      3],
		[54, 'Jonathan',   2],
		[55, 'Brandon',    1],
	];

	/**
	 * @var array
	 */
	private $dataRows;

	/**
	 * @var \PDO
	 */
	private $pdo;

	/**
	 * @var mef\Sql\Driver\SqlDriver
	 */
	private $db;

	/**
	 * @var mef\Sql\Writer\SqliteWriter
	 */
	private $writer;

	public function setup() : void
	{
		$this->dataRows = array_map(function($row)
		{
			return [
				'id' => (string) $row[0],
				'name' => $row[1],
				'age' => (string) $row[2]
			];
		}, self::DATA);

		$this->pdo = new PDO('sqlite::memory:');
		$db = new mef\Db\Driver\PdoDriver($this->pdo);
		$this->writer = new mef\Sql\Writer\SqliteWriter($db);
		$this->db = new mef\Sql\Driver\SqlDriver($this->writer);

		$this->db->execute('CREATE TABLE person (
			id INTEGER,
			name TEXT,
			age INTEGER
		)');

		$st = $this->db->prepare('INSERT INTO person VALUES (:id, :name, :age)');
		$st->bindParameters([
			':id' => &$id,
			':name' => &$name,
			':age' => &$age
		]);

		foreach(self::DATA as list($id, $name, $age))
		{
			$st->execute();
		}
	}

	public function testPaginateByPagesAscending()
	{
		$PAGE_SIZE = 10;

		$paginator = new Paginator($PAGE_SIZE);
		$query = $this->db->select('*')->from('person');

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0, 20);
		$paginatedQuery = $pagination->paginate($query);
		$firstPagePageSizeTwenty = $paginatedQuery->query()->fetchAll();
		$firstPagePageSizeTwentyPaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0, $PAGE_SIZE, true);
		$paginatedQuery = $pagination->paginate($query);
		$firstPageInverted = $paginatedQuery->query()->fetchAll();
		$firstPageInvertedPaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0);
		$paginatedQuery = $pagination->paginate($query);
		$firstPage = $paginatedQuery->query()->fetchAll();
		$firstPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 1);
		$paginatedQuery = $pagination->paginate($query);
		$secondPage = $paginatedQuery->query()->fetchAll();
		$secondPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 2);
		$paginatedQuery = $pagination->paginate($query);
		$thirdPage = $paginatedQuery->query()->fetchAll();
		$thirdPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 3);
		$paginatedQuery = $pagination->paginate($query);
		$fourthPage = $paginatedQuery->query()->fetchAll();
		$fourthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 4);
		$paginatedQuery = $pagination->paginate($query);
		$fifthPage = $paginatedQuery->query()->fetchAll();
		$fifthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 5);
		$paginatedQuery = $pagination->paginate($query);
		$sixthPage = $paginatedQuery->query()->fetchAll();
		$sixthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 5, $PAGE_SIZE, true);
		$paginatedQuery = $pagination->paginate($query);
		$sixthPageInverted = $paginatedQuery->query()->fetchAll();
		$sixthPageInvertedPaginationStats = $paginatedQuery->getStats();

		// Assert page stats are correct
		$this->assertTrue($firstPagePageSizeTwentyPaginationStats['containsFirst']);
		$this->assertTrue($firstPageInvertedPaginationStats['containsFirst']);
		$this->assertTrue($firstPagePaginationStats['containsFirst']);
		$this->assertFalse($secondPagePaginationStats['containsFirst']);
		$this->assertFalse($thirdPagePaginationStats['containsFirst']);
		$this->assertFalse($fourthPagePaginationStats['containsFirst']);
		$this->assertFalse($fifthPagePaginationStats['containsFirst']);
		$this->assertFalse($sixthPagePaginationStats['containsFirst']);
		$this->assertFalse($sixthPageInvertedPaginationStats['containsFirst']);

		$this->assertFalse($firstPagePageSizeTwentyPaginationStats['containsLast']);
		$this->assertFalse($firstPageInvertedPaginationStats['containsLast']);
		$this->assertFalse($firstPagePaginationStats['containsLast']);
		$this->assertFalse($secondPagePaginationStats['containsLast']);
		$this->assertFalse($thirdPagePaginationStats['containsLast']);
		$this->assertFalse($fourthPagePaginationStats['containsLast']);
		$this->assertFalse($fifthPagePaginationStats['containsLast']);
		$this->assertTrue($sixthPagePaginationStats['containsLast']);
		$this->assertTrue($sixthPageInvertedPaginationStats['containsLast']);

		$this->assertEquals(35, $firstPagePageSizeTwentyPaginationStats['remainingCount']);
		$this->assertEquals(45, $firstPageInvertedPaginationStats['remainingCount']);
		$this->assertEquals(45, $firstPagePaginationStats['remainingCount']);
		$this->assertEquals(35, $secondPagePaginationStats['remainingCount']);
		$this->assertEquals(25, $thirdPagePaginationStats['remainingCount']);
		$this->assertEquals(15, $fourthPagePaginationStats['remainingCount']);
		$this->assertEquals(5, $fifthPagePaginationStats['remainingCount']);
		$this->assertEquals(0, $sixthPagePaginationStats['remainingCount']);
		$this->assertEquals(0, $sixthPageInvertedPaginationStats['remainingCount']);

		// Assert page sizes are correct
		$this->assertEquals(20, count($firstPagePageSizeTwenty));
		$this->assertEquals(10, count($firstPageInverted));
		$this->assertEquals(10, count($firstPage));
		$this->assertEquals(10, count($secondPage));
		$this->assertEquals(10, count($thirdPage));
		$this->assertEquals(10, count($fourthPage));
		$this->assertEquals(10, count($fifthPage));
		$this->assertEquals(5, count($sixthPage));
		$this->assertEquals(5, count($sixthPageInverted));

		// Assert page contents are correct
		$this->assertEquals($this->dataRows[54], $firstPagePageSizeTwenty[0]);
		$this->assertEquals($this->dataRows[35], $firstPagePageSizeTwenty[19]);
		$this->assertEquals($this->dataRows[0], $firstPageInverted[0]);
		$this->assertEquals($this->dataRows[9], $firstPageInverted[9]);
		$this->assertEquals($this->dataRows[54], $firstPage[0]);
		$this->assertEquals($this->dataRows[45], $firstPage[9]);
		$this->assertEquals($this->dataRows[44], $secondPage[0]);
		$this->assertEquals($this->dataRows[35], $secondPage[9]);
		$this->assertEquals($this->dataRows[34], $thirdPage[0]);
		$this->assertEquals($this->dataRows[25], $thirdPage[9]);
		$this->assertEquals($this->dataRows[24], $fourthPage[0]);
		$this->assertEquals($this->dataRows[15], $fourthPage[9]);
		$this->assertEquals($this->dataRows[14], $fifthPage[0]);
		$this->assertEquals($this->dataRows[5], $fifthPage[9]);
		$this->assertEquals($this->dataRows[4], $sixthPage[0]);
		$this->assertEquals($this->dataRows[0], $sixthPage[4]);
		$this->assertEquals($this->dataRows[50], $sixthPageInverted[0]);
		$this->assertEquals($this->dataRows[54], $sixthPageInverted[4]);
	}

	public function testRelativePaginationAscending()
	{
		$PAGE_SIZE = 10;

		$paginator = new Paginator($PAGE_SIZE);
		$query = $this->db->select('*')->from('person');

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0);
		$paginatedQuery = $pagination->paginate($query);
		$firstPage = $paginatedQuery->query()->fetchAll();
		$firstPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($firstPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$secondPage = $paginatedQuery->query()->fetchAll();
		$secondPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($secondPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$thirdPage = $paginatedQuery->query()->fetchAll();
		$thirdPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($thirdPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$fourthPage = $paginatedQuery->query()->fetchAll();
		$fourthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($fourthPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$fifthPage = $paginatedQuery->query()->fetchAll();
		$fifthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($fifthPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$sixthPage = $paginatedQuery->query()->fetchAll();
		$sixthPagePaginationStats = $paginatedQuery->getStats();

		// Assert page stats are correct
		$this->assertTrue($firstPagePaginationStats['containsFirst']);
		$this->assertFalse($secondPagePaginationStats['containsFirst']);
		$this->assertFalse($thirdPagePaginationStats['containsFirst']);
		$this->assertFalse($fourthPagePaginationStats['containsFirst']);
		$this->assertFalse($fifthPagePaginationStats['containsFirst']);
		$this->assertFalse($sixthPagePaginationStats['containsFirst']);

		$this->assertFalse($firstPagePaginationStats['containsLast']);
		$this->assertFalse($secondPagePaginationStats['containsLast']);
		$this->assertFalse($thirdPagePaginationStats['containsLast']);
		$this->assertFalse($fourthPagePaginationStats['containsLast']);
		$this->assertFalse($fifthPagePaginationStats['containsLast']);
		$this->assertTrue($sixthPagePaginationStats['containsLast']);

		$this->assertEquals(45, $firstPagePaginationStats['remainingCount']);
		$this->assertEquals(35, $secondPagePaginationStats['remainingCount']);
		$this->assertEquals(25, $thirdPagePaginationStats['remainingCount']);
		$this->assertEquals(15, $fourthPagePaginationStats['remainingCount']);
		$this->assertEquals(5, $fifthPagePaginationStats['remainingCount']);
		$this->assertEquals(0, $sixthPagePaginationStats['remainingCount']);

		// Assert page sizes are correct
		$this->assertEquals(10, count($firstPage));
		$this->assertEquals(10, count($secondPage));
		$this->assertEquals(10, count($thirdPage));
		$this->assertEquals(10, count($fourthPage));
		$this->assertEquals(10, count($fifthPage));
		$this->assertEquals(5, count($sixthPage));

		// Assert page contents are correct
		$this->assertEquals($this->dataRows[54], $firstPage[0]);
		$this->assertEquals($this->dataRows[45], $firstPage[9]);
		$this->assertEquals($this->dataRows[44], $secondPage[0]);
		$this->assertEquals($this->dataRows[35], $secondPage[9]);
		$this->assertEquals($this->dataRows[34], $thirdPage[0]);
		$this->assertEquals($this->dataRows[25], $thirdPage[9]);
		$this->assertEquals($this->dataRows[24], $fourthPage[0]);
		$this->assertEquals($this->dataRows[15], $fourthPage[9]);
		$this->assertEquals($this->dataRows[14], $fifthPage[0]);
		$this->assertEquals($this->dataRows[5], $fifthPage[9]);
		$this->assertEquals($this->dataRows[4], $sixthPage[0]);
		$this->assertEquals($this->dataRows[0], $sixthPage[4]);
	}

	public function testRelativePaginationDescending()
	{
		$PAGE_SIZE = 10;

		$paginator = new Paginator($PAGE_SIZE);
		$query = $this->db->select('*')->from('person');

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 5);
		$paginatedQuery = $pagination->paginate($query);
		$sixthPage = $paginatedQuery->query()->fetchAll();
		$sixthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($sixthPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$fifthPage = $paginatedQuery->query()->fetchAll();
		$fifthPagePaginationStats = $paginatedQuery->getStats();	

		$pagination = $paginator->before(reset($fifthPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$fourthPage = $paginatedQuery->query()->fetchAll();
		$fourthPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($fourthPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$thirdPage = $paginatedQuery->query()->fetchAll();
		$thirdPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($thirdPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$secondPage = $paginatedQuery->query()->fetchAll();
		$secondPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($secondPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$firstPage = $paginatedQuery->query()->fetchAll();
		$firstPagePaginationStats = $paginatedQuery->getStats();

		// Assert page stats are correct
		$this->assertTrue($firstPagePaginationStats['containsFirst']);
		$this->assertFalse($secondPagePaginationStats['containsFirst']);
		$this->assertFalse($thirdPagePaginationStats['containsFirst']);
		$this->assertFalse($fourthPagePaginationStats['containsFirst']);
		$this->assertFalse($fifthPagePaginationStats['containsFirst']);
		$this->assertFalse($sixthPagePaginationStats['containsFirst']);

		$this->assertFalse($firstPagePaginationStats['containsLast']);
		$this->assertFalse($secondPagePaginationStats['containsLast']);
		$this->assertFalse($thirdPagePaginationStats['containsLast']);
		$this->assertFalse($fourthPagePaginationStats['containsLast']);
		$this->assertFalse($fifthPagePaginationStats['containsLast']);
		$this->assertTrue($sixthPagePaginationStats['containsLast']);

		$this->assertEquals(45, $firstPagePaginationStats['remainingCount']);
		$this->assertEquals(35, $secondPagePaginationStats['remainingCount']);
		$this->assertEquals(25, $thirdPagePaginationStats['remainingCount']);
		$this->assertEquals(15, $fourthPagePaginationStats['remainingCount']);
		$this->assertEquals(5, $fifthPagePaginationStats['remainingCount']);
		$this->assertEquals(0, $sixthPagePaginationStats['remainingCount']);

		// Assert page sizes are correct
		$this->assertEquals(10, count($firstPage));
		$this->assertEquals(10, count($secondPage));
		$this->assertEquals(10, count($thirdPage));
		$this->assertEquals(10, count($fourthPage));
		$this->assertEquals(10, count($fifthPage));
		$this->assertEquals(5, count($sixthPage));

		// Assert page contents are correct
		$this->assertEquals($this->dataRows[54], $firstPage[0]);
		$this->assertEquals($this->dataRows[45], $firstPage[9]);
		$this->assertEquals($this->dataRows[44], $secondPage[0]);
		$this->assertEquals($this->dataRows[35], $secondPage[9]);
		$this->assertEquals($this->dataRows[34], $thirdPage[0]);
		$this->assertEquals($this->dataRows[25], $thirdPage[9]);
		$this->assertEquals($this->dataRows[24], $fourthPage[0]);
		$this->assertEquals($this->dataRows[15], $fourthPage[9]);
		$this->assertEquals($this->dataRows[14], $fifthPage[0]);
		$this->assertEquals($this->dataRows[5], $fifthPage[9]);
		$this->assertEquals($this->dataRows[4], $sixthPage[0]);
		$this->assertEquals($this->dataRows[0], $sixthPage[4]);
	}

	public function testPaginationStatsFirstPage()
	{
		$PAGE_SIZE = 25;

		$paginator = new Paginator($PAGE_SIZE);
		$query = $this->db->select('*')->from('person');

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0);
		$paginatedQuery = $pagination->paginate($query);
		$firstPage = $paginatedQuery->query()->fetchAll();
		$firstPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($firstPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$secondPage = $paginatedQuery->query()->fetchAll();
		$secondPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($secondPage), ['age', 'id' => 'DESC']);
		$paginatedQuery = $pagination->paginate($query);
		$firstPageUsingBefore = $paginatedQuery->query()->fetchAll();
		$firstPageUsingBeforePaginationStats = $paginatedQuery->getStats();

		$this->assertTrue($firstPagePaginationStats['containsFirst']);
		$this->assertFalse($secondPagePaginationStats['containsFirst']);
		$this->assertTrue($firstPageUsingBeforePaginationStats['containsFirst']);

		$this->assertFalse($firstPagePaginationStats['containsLast']);
		$this->assertFalse($secondPagePaginationStats['containsLast']);
		$this->assertFalse($firstPageUsingBeforePaginationStats['containsLast']);

		$this->assertEquals(30, $firstPagePaginationStats['remainingCount']);
		$this->assertEquals(5, $secondPagePaginationStats['remainingCount']);
		$this->assertEquals(30, $firstPageUsingBeforePaginationStats['remainingCount']);

		$this->assertEquals($firstPage, $firstPageUsingBefore);
		$this->assertEquals($firstPagePaginationStats, $firstPageUsingBeforePaginationStats);
	}

	public function testPaginationStatsFirstPageInvertedOrder()
	{
		$PAGE_SIZE = 25;

		$paginator = new Paginator($PAGE_SIZE);
		$query = $this->db->select('*')->from('person');

		$pagination = $paginator->page(['age', 'id' => 'DESC'], 0, $PAGE_SIZE, true);
		$paginatedQuery = $pagination->paginate($query);
		$firstPage = $paginatedQuery->query()->fetchAll();
		$firstPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->after(end($firstPage), ['age', 'id' => 'DESC'], $PAGE_SIZE, true);
		$paginatedQuery = $pagination->paginate($query);
		$secondPage = $paginatedQuery->query()->fetchAll();
		$secondPagePaginationStats = $paginatedQuery->getStats();

		$pagination = $paginator->before(reset($secondPage), ['age', 'id' => 'DESC'], $PAGE_SIZE, true);
		$paginatedQuery = $pagination->paginate($query);
		$firstPageUsingBefore = $paginatedQuery->query()->fetchAll();
		$firstPageUsingBeforePaginationStats = $paginatedQuery->getStats();

		$this->assertTrue($firstPagePaginationStats['containsFirst']);
		$this->assertFalse($secondPagePaginationStats['containsFirst']);
		$this->assertTrue($firstPageUsingBeforePaginationStats['containsFirst']);

		$this->assertFalse($firstPagePaginationStats['containsLast']);
		$this->assertFalse($secondPagePaginationStats['containsLast']);
		$this->assertFalse($firstPageUsingBeforePaginationStats['containsLast']);

		$this->assertEquals(30, $firstPagePaginationStats['remainingCount']);
		$this->assertEquals(5, $secondPagePaginationStats['remainingCount']);
		$this->assertEquals(30, $firstPageUsingBeforePaginationStats['remainingCount']);

		$this->assertEquals($firstPage, $firstPageUsingBefore);
		$this->assertEquals($firstPagePaginationStats, $firstPageUsingBeforePaginationStats);
	}
}
