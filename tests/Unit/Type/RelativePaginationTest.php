<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Type;

use mef\Pagination\Type\{RelativePagination, PaginationField};
use mef\Pagination\Sql\RelativePaginatedQuery;

use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Writer\AbstractWriter;

use mef\Db\Driver\DriverInterface;


/**
 * @coversDefaultClass \mef\Pagination\Type\RelativePagination
 */
class RelativePaginationTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var int
	 */
	private $pageSize;

	/**
	 * @var bool
	 */
	private $isReversed;

	public function setup() : void
	{
		$this->fields = [
			new PaginationField('name', '', PaginationField::DESC),
			new PaginationField('id', '', PaginationField::ASC)
		];

		$this->pageSize = 10;
		$this->isReversed = true;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$pagination = new RelativePagination($this->fields, $this->pageSize, $this->isReversed);

		$this->assertTrue($pagination instanceof RelativePagination);

		return $pagination;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidFields()
	{
		$this->expectException(\InvalidArgumentException::class);
		$pagination = new RelativePagination(['foo'], $this->pageSize, $this->isReversed);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidPageSize()
	{
		$this->expectException(\InvalidArgumentException::class);
		$pagination = new RelativePagination($this->fields, 0, $this->isReversed);
	}

	/**
	 * @covers ::isReversed
	 * @depends testConstructor
	 */
	public function testIsReversed(RelativePagination $pagination)
	{
		$this->assertEquals($this->isReversed, $pagination->isReversed());
	}

	/**
	 * @covers ::paginate
	 * @depends testConstructor
	 */
	public function testPaginate(RelativePagination $pagination)
	{
		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();

		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();

		$mockSelectBuilder = $this->getMockBuilder(SelectBuilder::class)->getMock();
		$mockSelectBuilder->expects($this->any())->
			method('getWriter')->
			withAnyParameters()->
			willReturn($mockAbstractWriter);

		$offsetPaginatedQuery = $pagination->paginate($mockSelectBuilder);

		$this->assertTrue($offsetPaginatedQuery instanceof RelativePaginatedQuery);
	}
}