<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Type;

use mef\Pagination\Type\{OffsetPagination, PaginationField};
use mef\Pagination\Sql\OffsetPaginatedQuery;

use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Writer\AbstractWriter;

use mef\Db\Driver\DriverInterface;


/**
 * @coversDefaultClass \mef\Pagination\Type\OffsetPagination
 */
class OffsetPaginationTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var int
	 */
	private $pageSize;

	/**
	 * @var int
	 */
	private $offset;

	public function setup() : void
	{
		$this->fields = [
			new PaginationField('name', '', PaginationField::DESC),
			new PaginationField('id', '', PaginationField::ASC)
		];

		$this->pageSize = 10;
		$this->offset = 5;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$pagination = new OffsetPagination($this->fields, $this->pageSize, $this->offset);

		$this->assertTrue($pagination instanceof OffsetPagination);

		return $pagination;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidFields()
	{
		$this->expectException(\InvalidArgumentException::class);
		$pagination = new OffsetPagination(['foo'], $this->pageSize, $this->offset);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidPageSize()
	{
		$this->expectException(\InvalidArgumentException::class);
		$pagination = new OffsetPagination($this->fields, 0, $this->offset);
	}

	/**
	 * @covers ::getOffset
	 * @depends testConstructor
	 */
	public function testGetOffset(OffsetPagination $pagination)
	{
		$this->assertEquals($this->offset, $pagination->getOffset());
	}

	/**
	 * @covers ::paginate
	 * @depends testConstructor
	 */
	public function testPaginate(OffsetPagination $pagination)
	{
		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();

		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();

		$mockSelectBuilder = $this->getMockBuilder(SelectBuilder::class)->getMock();
		$mockSelectBuilder->expects($this->any())->
			method('getWriter')->
			withAnyParameters()->
			willReturn($mockAbstractWriter);

		$offsetPaginatedQuery = $pagination->paginate($mockSelectBuilder);

		$this->assertTrue($offsetPaginatedQuery instanceof OffsetPaginatedQuery);
	}
}