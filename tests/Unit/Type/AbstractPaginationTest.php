<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Type;

use mef\Pagination\Paginator;
use mef\Pagination\Type\{AbstractPagination, PaginationField};

/**
 * @coversDefaultClass \mef\Pagination\Type\AbstractPagination
 */
class AbstractPaginationTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var array
	 */
	private $fields;

	/**
	 * @var int
	 */
	private $size;

	public function setup() : void
	{
		$this->fields = [
			new PaginationField('name', '', PaginationField::DESC),
			new PaginationField('id', '', PaginationField::ASC)
		];

		$this->size = 10;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$pagination = $this->getMockBuilder(AbstractPagination::class)->
			setMethods(['paginate'])->
			setConstructorArgs([$this->fields, $this->size])->
			getMock();

		$this->assertTrue($pagination instanceof AbstractPagination);

		return $pagination;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidFields()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->getMockBuilder(AbstractPagination::class)->
			setConstructorArgs([['foo'], 0])->
			getMock();
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidPageSize()
	{
		$this->expectException(\InvalidArgumentException::class);
		$this->getMockBuilder(AbstractPagination::class)->
			setConstructorArgs([$this->fields, 0])->
			getMock();
	}

	/**
	 * @covers ::getFields
	 * @depends testConstructor
	 */
	public function testGetFields(AbstractPagination $pagination)
	{
		$this->assertEquals($this->fields, $pagination->getFields());
	}

	/**
	 * @covers ::getPageSize
	 * @depends testConstructor
	 */
	public function testGetPageSize(AbstractPagination $pagination)
	{
		$this->assertSame($this->size, $pagination->getPageSize());
	}

	/**
	 * @covers ::mapFieldNames
	 * @depends testConstructor
	 */
	public function testMapFieldNames(AbstractPagination $pagination)
	{
		$mapped = $pagination->mapFieldNames([]);

		// nothing was mapped, so it should return same object
		$this->assertSame($mapped, $pagination);

		$mapped = $pagination->mapFieldNames(['name' => 'tbl.name']);

		$this->assertNotSame($mapped, $pagination);
		$this->assertCount(2, $mapped->getFields());
		$this->assertNotSame($mapped->getFields()[0], $pagination->getFields()[0]);
		$this->assertSame($mapped->getFields()[1], $pagination->getFields()[1]);
		$this->assertSame('tbl.name', $mapped->getFields()[0]->getName());
	}
}