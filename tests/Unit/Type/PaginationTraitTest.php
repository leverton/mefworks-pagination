<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Type;

use mef\Pagination\Type\{PaginationInterface, PaginationTrait};

/**
 * @coversDefaultClass \mef\Pagination\Type\PaginationTrait
 */
class PaginationTraitTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var array
	 */
	private $optionsEmpty;

	/**
	 * @var array
	 */
	private $optionsWithPagination;

	/**
	 * @var array
	 */
	private $optionsWithPaginationStats;

	/**
	 * @var array
	 */
	private $optionsWithPaginationAndPaginationStats;

	public function setup() : void
	{
		$mockPaginationInterface = $this->getMockForAbstractClass(PaginationInterface::class);

		$this->optionsEmpty = [];
		$this->optionsWithPagination = [ 'pagination' => $mockPaginationInterface ];
		$this->optionsWithPaginationStats = [ 'pagination_stats' => true ];
		$this->optionsWithPaginationAndPaginationStats = [ 'pagination' => $mockPaginationInterface, 'pagination_stats' => true ];
	}

	private function getPaginationTraitWithEmptyOptions()
	{
		$mockTrait = $this->getMockForTrait(PaginationTrait::class);

		$reflection = new \ReflectionClass($mockTrait);
		$property = $reflection->getProperty('options');
		$property->setAccessible(true);
		$property->setValue($mockTrait, $this->optionsEmpty);

		return $mockTrait;
	}

	private function getPaginationTraitWithPagination()
	{
		$mockTrait = $this->getMockForTrait(PaginationTrait::class);

		$reflection = new \ReflectionClass($mockTrait);
		$property = $reflection->getProperty('options');
		$property->setAccessible(true);
		$property->setValue($mockTrait, $this->optionsWithPagination);

		return $mockTrait;
	}

	private function getPaginationTraitWithPaginationStats()
	{
		$mockTrait = $this->getMockForTrait(PaginationTrait::class);

		$reflection = new \ReflectionClass($mockTrait);
		$property = $reflection->getProperty('options');
		$property->setAccessible(true);
		$property->setValue($mockTrait, $this->optionsWithPaginationStats);

		return $mockTrait;
	}

	private function getPaginationTraitWithPaginationAndPaginationStats()
	{
		$mockTrait = $this->getMockForTrait(PaginationTrait::class);

		$reflection = new \ReflectionClass($mockTrait);
		$property = $reflection->getProperty('options');
		$property->setAccessible(true);
		$property->setValue($mockTrait, $this->optionsWithPaginationAndPaginationStats);

		return $mockTrait;
	}

	/**
	 * @covers ::hasPagination
	 */
	public function testHasPagination()
	{
		$trait = $this->getPaginationTraitWithPagination();
		$this->assertEquals(true, $trait->hasPagination());
	}

	/**
	 * @covers ::hasPagination
	 */
	public function testHasPaginationWithEmptyOptions()
	{
		$trait = $this->getPaginationTraitWithEmptyOptions();
		$this->assertEquals(false, $trait->hasPagination());
	}

	/**
	 * @covers ::wantsPaginatedStats
	 */
	public function testWantsPaginatedStats()
	{
		$trait = $this->getPaginationTraitWithPaginationAndPaginationStats();
		$this->assertEquals(true, $trait->wantsPaginatedStats());
	}

	/**
	 * @covers ::wantsPaginatedStats
	 */
	public function testWantsPaginatedStatsWithoutPagination()
	{
		$trait = $this->getPaginationTraitWithPaginationStats();
		$this->assertEquals(false, $trait->wantsPaginatedStats());
	}

	/**
	 * @covers ::wantsPaginatedStats
	 */
	public function testWantsPaginatedStatsWithEmptyOptions()
	{
		$trait = $this->getPaginationTraitWithEmptyOptions();
		$this->assertEquals(false, $trait->wantsPaginatedStats());
	}

	/**
	 * @covers ::getPagination
	 */
	public function testGetPagination()
	{
		$trait = $this->getPaginationTraitWithPagination();
		$this->assertEquals($this->optionsWithPagination['pagination'], $trait->getPagination());
	}

	/**
	 * @covers ::getPagination
	 */
	public function testGetPaginationWithEmptyOptions()
	{
		$trait = $this->getPaginationTraitWithEmptyOptions();
		$this->expectException(\RuntimeException::class);
		$trait->getPagination();
	}
}