<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Type;

use mef\Pagination\Type\PaginationField;

/**
 * @coversDefaultClass \mef\Pagination\Type\PaginationField
 */
class PaginationFieldTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var string
	 */
	private $name;

	/**
	 * @var string
	 */
	private $value;

	/**
	 * @var string
	 */
	private $order;

	public function setup() : void
	{
		$this->name = 'id';
		$this->value = '';
		$this->order = 'DESC';
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$field = new PaginationField($this->name, $this->value, $this->order);

		$this->assertTrue($field instanceof PaginationField);

		return $field;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidName()
	{
		$this->expectException(\InvalidArgumentException::class);
		$field = new PaginationField('', $this->value, $this->order);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithInvalidOrder()
	{
		$this->expectException(\InvalidArgumentException::class);
		$field = new PaginationField($this->name, $this->value, '');
	}

	/**
	 * @covers ::getName
	 * @depends testConstructor
	 */
	public function testGetName(PaginationField $pagination)
	{
		$this->assertEquals($this->name, $pagination->getName());
	}

	/**
	 * @covers ::getValue
	 * @depends testConstructor
	 */
	public function testGetValue(PaginationField $pagination)
	{
		$this->assertEquals($this->value, $pagination->getValue());
	}

	/**
	 * @covers ::isAscending
	 * @depends testConstructor
	 */
	public function testIsAscending(PaginationField $pagination)
	{
		$this->assertEquals($this->order == 'ASC', $pagination->isAscending());
	}
}