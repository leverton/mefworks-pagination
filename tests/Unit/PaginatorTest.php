<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit;

use mef\Pagination\Paginator;
use mef\Pagination\Type\{OffsetPagination, PaginationField, RelativePagination, PaginationInterface};

/**
 * @coversDefaultClass \mef\Pagination\Paginator
 */
class PaginatorTest extends \PHPUnit\Framework\TestCase
{
	const DEFAULT_PAGE_SIZE = 20;

	/**
	 * @var array
	 */
	private $reference = [
		'id' => 1,
		'name' => 'John Doe',
		'age' => 42
	];

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$paginator = new Paginator(self::DEFAULT_PAGE_SIZE);

		$this->assertTrue($paginator instanceof Paginator);

		return $paginator;
	}

	/**
	 * @covers ::__construct
	 */
	public function testInvalidPageSizeInConstructor()
	{
		$this->expectException(\InvalidArgumentException::class);
		$paginator = new Paginator(-1);
	}

	/**
	 * @depends testConstructor
	 * @covers ::before
	 * @covers ::createPagination
	 */
	public function testBefore(Paginator $paginator)
	{
		$pagination = $paginator->before($this->reference, ['name' => PaginationField::DESC, 'id']);

		$this->assertTrue($pagination instanceof RelativePagination);

		// Because we are doing a before(), ASC becomes DESC, and DESC becomes ASC.

		$this->assertTrue($pagination->isReversed());
		$this->assertCount(2, $pagination->getFields());
		$this->assertTrue($pagination->getFields()[0]->isAscending());
		$this->assertFalse($pagination->getFields()[1]->isAscending());
		$this->assertSame('name', $pagination->getFields()[0]->getName());
		$this->assertSame('id', $pagination->getFields()[1]->getName());
	}

	/**
	 * @depends testConstructor
	 * @covers ::after
	 * @covers ::createPagination
	 */
	public function testBeforeWithInversion(Paginator $paginator)
	{
		$pagination = $paginator->before($this->reference, ['name', 'id'], 0, true);

		$this->assertTrue($pagination instanceof RelativePagination);

		// Because we are doing a before() WITH inversion, the ASC remains ASC.
		// However, the results itself still must be reversed.

		$this->assertTrue($pagination->isReversed());
		$this->assertCount(2, $pagination->getFields());
		$this->assertTrue($pagination->getFields()[0]->isAscending());
		$this->assertTrue($pagination->getFields()[1]->isAscending());
	}

	/**
	 * @depends testConstructor
	 * @covers ::before
	 * @covers ::createPagination
	 */
	public function testBeforeWithInvalidReference(Paginator $paginator)
	{
		$this->expectException(\InvalidArgumentException::class);
		$paginator->before($this->reference, ['foo']);
	}

	/**
	 * @depends testConstructor
	 * @covers ::after
	 * @covers ::createPagination
	 */
	public function testAfter(Paginator $paginator)
	{
		$pagination = $paginator->after($this->reference, ['name' => PaginationField::DESC, 'id']);

		$this->assertTrue($pagination instanceof RelativePagination);

		$this->assertFalse($pagination->isReversed());
		$this->assertCount(2, $pagination->getFields());
		$this->assertFalse($pagination->getFields()[0]->isAscending());
		$this->assertTrue($pagination->getFields()[1]->isAscending());
		$this->assertSame('name', $pagination->getFields()[0]->getName());
		$this->assertSame('id', $pagination->getFields()[1]->getName());
	}

	/**
	 * @depends testConstructor
	 * @covers ::after
	 * @covers ::createPagination
	 */
	public function testAfterWithInversion(Paginator $paginator)
	{
		$pagination = $paginator->after($this->reference, ['name', 'id'], 0, true);

		$this->assertTrue($pagination instanceof RelativePagination);

		$this->assertFalse($pagination->isReversed());
		$this->assertCount(2, $pagination->getFields());
		$this->assertFalse($pagination->getFields()[0]->isAscending());
		$this->assertFalse($pagination->getFields()[1]->isAscending());
	}

	/**
	 * @depends testConstructor
	 * @covers ::page
	 */
	public function testPage(Paginator $paginator)
	{
		$pagination = $paginator->page(['foo', 'bar'], 0);

		$this->assertTrue($pagination instanceof OffsetPagination);
	}

	/**
	 * @depends testConstructor
	 * @covers ::page
	 */
	public function testPageWithInvalidPageNumber(Paginator $paginator)
	{
		$this->expectException(\InvalidArgumentException::class);
		$paginator->page(['foo', 'bar'], -1);
	}

	/**
	 * @depends testConstructor
	 * @covers ::page
	 */
	public function testPageWithInvalidPageSize(Paginator $paginator)
	{
		$this->expectException(\InvalidArgumentException::class);
		$paginator->page(['foo', 'bar'], 0, -1);
	}

	/**
	 * @depends testConstructor
	 * @covers ::page
	 */
	public function testReversedPage(Paginator $paginator)
	{
		$pagination = $paginator->page(['foo', 'bar'], 0, 0, true);

		$this->assertTrue($pagination instanceof OffsetPagination);
	}

	/**
	 * @depends testConstructor
	 * @covers ::page
	 */
	public function testPaginatorInterface(Paginator $paginator)
	{
		$pagination = $paginator->page(['foo', 'bar'], 0, 0, true);

		$this->assertTrue($pagination instanceof PaginationInterface);
	}

	/**
	 * @depends testConstructor
	 * @covers ::after
	 * @covers ::createPagination
	 */
	public function testIndexedReference(Paginator $paginator)
	{
		$pagination = $paginator->after([$this->reference['name'], $this->reference['id']], ['name', 'id'], 0, true);

		$this->assertTrue($pagination instanceof RelativePagination);
	}

	/**
	 * @depends testConstructor
	 * @covers ::after
	 * @covers ::createPagination
	 */
	public function testIndexedReferenceWithWrongNumberOfReferenceFields(Paginator $paginator)
	{
		$this->expectException(\InvalidArgumentException::class);
		$pagination = $paginator->after(array_values($this->reference), ['name', 'id'], 0, true);
	}
}