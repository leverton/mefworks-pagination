<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Sql;

use mef\Pagination\Sql\{OffsetPaginatedQuery, AbstractPaginatedQuery};
use mef\Pagination\Type\{OffsetPagination, PaginationField};

use mef\Sql\PreparedQuery;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Writer\AbstractWriter;

use mef\Db\Statement\StatementInterface;
use mef\Db\Driver\DriverInterface;

use mef\Db\RecordSet\ArrayRecordSet;

use RuntimeException;

/**
 * @coversDefaultClass \mef\Pagination\Sql\OffsetPaginatedQuery
 */
class OffsetPaginatedQueryTest extends \PHPUnit\Framework\TestCase
{
	private function getOffsetPaginatedQueryForFirstPage()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$mockPaginationOffset = 0;
		$totalNumRecords = 13;

		$firstPageData = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5'],
			['id' => 6, 'name' => 'name6']
		]);

		$firstPageDataResponse = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($firstPageData);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(OffsetPagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'getOffset', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, $mockPaginationOffset])->
			getMock();

		$offsetPaginatedQuery = $this->getMockBuilder(OffsetPaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$offsetPaginatedQuery->method('count')->willReturn($totalNumRecords);

		return $offsetPaginatedQuery;
	}

	private function getOffsetPaginatedQueryForLastPage()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$mockPaginationOffset = 10;
		$totalNumRecords = 13;

		$lastPageData = new ArrayRecordSet([
			['id' => 10, 'name' => 'name10'],
			['id' => 11, 'name' => 'name11'],
			['id' => 12, 'name' => 'name12']
		]);

		$lastPageDataResponse = new ArrayRecordSet([
			['id' => 10, 'name' => 'name10'],
			['id' => 11, 'name' => 'name11'],
			['id' => 12, 'name' => 'name12']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($lastPageData);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(OffsetPagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'getOffset', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, $mockPaginationOffset])->
			getMock();

		$offsetPaginatedQuery = $this->getMockBuilder(OffsetPaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$offsetPaginatedQuery->method('count')->willReturn($totalNumRecords);

		return $offsetPaginatedQuery;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$query = $this->getOffsetPaginatedQueryForFirstPage();
		$this->assertTrue($query instanceof OffsetPaginatedQuery);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithLastPage()
	{
		$query = $this->getOffsetPaginatedQueryForLastPage();
		$this->assertTrue($query instanceof OffsetPaginatedQuery);
	}

	/**
	 * @covers ::query
	 */
	public function testQuery()
	{
		$expected = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5']
		]);

		$query = $this->getOffsetPaginatedQueryForFirstPage();
		$this->assertEquals($expected, $query->query());
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountBeforeQuery()
	{
		$this->expectException(RuntimeException::class);

		$query = $this->getOffsetPaginatedQueryForLastPage();
		$query->getRemainingCount();
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountWithFirstPage()
	{
		$expected = 8;

		$query = $this->getOffsetPaginatedQueryForFirstPage();
		$query->query();
		$this->assertEquals($expected, $query->getRemainingCount());
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountWithLastPage()
	{
		$expected = 0;

		$query = $this->getOffsetPaginatedQueryForLastPage();
		$query->query();
		$this->assertEquals($expected, $query->getRemainingCount());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithFirstPage()
	{
		$query = $this->getOffsetPaginatedQueryForFirstPage();
		$this->assertTrue($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithLastPage()
	{
		$query = $this->getOffsetPaginatedQueryForLastPage();
		$this->assertFalse($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordBeforeQuery()
	{
		$this->expectException(RuntimeException::class);

		$query = $this->getOffsetPaginatedQueryForLastPage();	
		$query->containsLastRecord();
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordWithLastPage()
	{
		$query = $this->getOffsetPaginatedQueryForLastPage();
		$query->query();
		$this->assertTrue($query->containsLastRecord());
	}
}