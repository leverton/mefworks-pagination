<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Sql;

use mef\Pagination\Sql\AbstractPaginatedQuery;
use mef\Pagination\Type\PaginationInterface;

use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Writer\AbstractWriter;

use mef\Db\Driver\DriverInterface;

/**
 * @coversDefaultClass \mef\Pagination\Sql\AbstractPaginatedQuery
 */
class AbstractPaginatedQueryTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @var \mef\Sql\Builder\SelectBuilder
	 */
	private $mockQuery;

	/**
	 * @var int
	 */
	private $mockQueryCount;

	/**
	 * @var \mef\Pagination\Type\PaginationInterface
	 */
	private $mockPagination;

	/**
	 * @var int
	 */
	private $mockPaginationPageSize;

	/**
	 * @var int
	 */
	private $remainingCount;

	/**
	 * @var bool
	 */
	private $containsLast;

	public function setup() : void
	{
		$this->mockQueryCount = 10;
		$this->mockPaginationPageSize = 10;
		$this->remainingCount = 20;
		$this->containsLast = false;

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();

		$this->mockQuery = $this->getMockBuilder(SelectBuilder::class)->getMock();

		$this->mockQuery->expects($this->any())->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$this->mockQuery->expects($this->any())->
			method('count')->
			withAnyParameters()->
			willReturn($this->mockQueryCount);

		$this->mockPagination = $this->getMockForAbstractClass(PaginationInterface::class);

		$this->mockPagination->expects($this->any())->
			method('getPageSize')->
			willReturn($this->mockPaginationPageSize);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$paginatedQuery = $this->getMockBuilder(AbstractPaginatedQuery::class)->
			setMethodsExcept(['getStats', 'getOriginalQuery', 'getPagination'])->
			setConstructorArgs([$this->mockQuery, $this->mockPagination])->
			getMock();

		$paginatedQuery->expects($this->any())->
			method('getRemainingCount')->
			willReturn($this->remainingCount);

		$paginatedQuery->expects($this->any())->
			method('containsLastRecord')->
			willReturn($this->containsLast);

		$this->assertTrue($paginatedQuery instanceof AbstractPaginatedQuery);

		return $paginatedQuery;
	}

	/**
	 * @covers ::getOriginalQuery
	 * @depends testConstructor
	 */
	public function testGetOriginalQuery(AbstractPaginatedQuery $query)
	{
		$this->assertEquals($this->mockQuery, $query->getOriginalQuery());
	}

	/**
	 * @covers ::getPagination
	 * @depends testConstructor
	 */
	public function testGetPagination(AbstractPaginatedQuery $query)
	{
		$pagination = $query->getPagination();

		$this->assertTrue($pagination instanceof PaginationInterface);
		$this->assertEquals($this->mockPaginationPageSize, $pagination->getPageSize());
	}

	/**
	 * @covers ::getStats
	 * @depends testConstructor
	 */
	public function testGetStats(AbstractPaginatedQuery $query)
	{
		$expected = [
			'pageSize' => $this->mockPaginationPageSize,
			'totalCount' => $this->mockQueryCount,
			'remainingCount' => $this->remainingCount,
			'containsFirst' => $this->mockPaginationPageSize + $this->remainingCount === $this->mockQueryCount,
			'containsLast' => $this->containsLast,
		];

		$this->assertEquals($expected, $query->getStats());
	}
}