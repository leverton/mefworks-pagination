<?php
declare(strict_types=1);

namespace mef\Pagination\Test\Unit\Sql;

use mef\Pagination\Sql\{RelativePaginatedQuery, AbstractPaginatedQuery};
use mef\Pagination\Type\{RelativePagination, PaginationField};

use mef\Sql\PreparedQuery;
use mef\Sql\Builder\SelectBuilder;
use mef\Sql\Writer\AbstractWriter;

use mef\Db\RecordSet\RecordSetInterface;
use mef\Db\Statement\StatementInterface;
use mef\Db\Driver\DriverInterface;

use mef\Db\RecordSet\ArrayRecordSet;

use RuntimeException;

/**
 * @coversDefaultClass \mef\Pagination\Sql\RelativePaginatedQuery
 */
class RelativePaginatedQueryTest extends \PHPUnit\Framework\TestCase
{
	private function getRelativePaginatedQueryFirstPage()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$totalNumRecords = 20;

		$firstPageData = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5'],
			['id' => 6, 'name' => 'name6']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($firstPageData);

		$mockRecordSetInterface = $this->getMockBuilder(RecordSetInterface::class)->getMock();
		$mockRecordSetInterface->
			method('fetchValue')->
			willReturn($totalNumRecords);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);
		$mockDriverInterface->
			method('query')->
			willReturn($mockRecordSetInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(RelativePagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, false])->
			getMock();

		$relativePaginatedQuery = $this->getMockBuilder(RelativePaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter', 'getOriginalQuery'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$relativePaginatedQuery->method('count')->willReturn($totalNumRecords);

		return $relativePaginatedQuery;
	}

	private function getRelativePaginatedQueryFirstPageReversed()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$totalNumRecords = 20;

		$firstPageData = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5'],
			['id' => 6, 'name' => 'name6']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($firstPageData);

		$mockRecordSetInterface = $this->getMockBuilder(RecordSetInterface::class)->getMock();
		$mockRecordSetInterface->
			method('fetchValue')->
			willReturn($totalNumRecords);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);
		$mockDriverInterface->
			method('query')->
			willReturn($mockRecordSetInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(RelativePagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, true])->
			getMock();

		$relativePaginatedQuery = $this->getMockBuilder(RelativePaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter', 'getOriginalQuery'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$relativePaginatedQuery->method('count')->willReturn($totalNumRecords);

		return $relativePaginatedQuery;
	}

	private function getRelativePaginatedQueryLastPage()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$totalNumRecords = 20;

		$lastPageData = new ArrayRecordSet([
			['id' => 17, 'name' => 'name17'],
			['id' => 18, 'name' => 'name18'],
			['id' => 19, 'name' => 'name19']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($lastPageData);

		$mockRecordSetInterface = $this->getMockBuilder(RecordSetInterface::class)->getMock();
		$mockRecordSetInterface->
			method('fetchValue')->
			willReturn($totalNumRecords);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);
		$mockDriverInterface->
			method('query')->
			willReturn($mockRecordSetInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(RelativePagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, false])->
			getMock();

		$relativePaginatedQuery = $this->getMockBuilder(RelativePaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter', 'getOriginalQuery'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$relativePaginatedQuery->method('count')->willReturn(3);

		return $relativePaginatedQuery;
	}

	private function getRelativePaginatedQueryLastPageReversed()
	{
		$mockPaginationFields = [
			new PaginationField('id', '', PaginationField::ASC),
			new PaginationField('name', '', PaginationField::DESC)
		];
		$mockPaginationPageSize = 5;
		$totalNumRecords = 20;

		$lastPageData = new ArrayRecordSet([
			['id' => 17, 'name' => 'name17'],
			['id' => 18, 'name' => 'name18'],
			['id' => 19, 'name' => 'name19']
		]);

		$mockStatementInterface = $this->getMockBuilder(StatementInterface::class)->getMock();
		$mockStatementInterface->
			method('query')->
			willReturn($lastPageData);

		$mockRecordSetInterface = $this->getMockBuilder(RecordSetInterface::class)->getMock();
		$mockRecordSetInterface->
			method('fetchValue')->
			willReturn($totalNumRecords);

		$mockDriverInterface = $this->getMockBuilder(DriverInterface::class)->getMock();
		$mockDriverInterface->
			method('prepare')->
			willReturn($mockStatementInterface);
		$mockDriverInterface->
			method('query')->
			willReturn($mockRecordSetInterface);

		$mockPreparedQuery = $this->getMockBuilder(PreparedQuery::class)->
			setMethodsExcept(['getSql', 'getValues'])->
			setConstructorArgs(['SELECT * FROM SOMETHING', []])->
			getMock();
		
		$mockAbstractWriter = $this->getMockBuilder(AbstractWriter::class)->setConstructorArgs([$mockDriverInterface])->getMock();
		$mockAbstractWriter->
			method('getDatabaseDriver')->
			willReturn($mockDriverInterface);
		$mockAbstractWriter->
			method('getPreparedSelect')->
			willReturn($mockPreparedQuery);

		$mockQuery = $this->getMockBuilder(SelectBuilder::class)->setMethods(['setWriter', 'getWriter'])->getMock();
		$mockQuery->
			method('getWriter')->
			willReturn($mockAbstractWriter);

		$mockPagination = $this->getMockBuilder(RelativePagination::class)->
			setMethodsExcept(['getFields', 'getPageSize', 'isReversed'])->
			setConstructorArgs([$mockPaginationFields, $mockPaginationPageSize, true])->
			getMock();

		$relativePaginatedQuery = $this->getMockBuilder(RelativePaginatedQuery::class)->
			setMethodsExcept(['query', 'getRemainingCount', 'containsFirstRecord', 'containsLastRecord', 'setWriter', 'getWriter', 'getOriginalQuery'])->
			setConstructorArgs([$mockQuery, $mockPagination])->
			getMock();
		$relativePaginatedQuery->method('count')->willReturn(3);

		return $relativePaginatedQuery;
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$query = $this->getRelativePaginatedQueryFirstPage();
		$this->assertTrue($query instanceof RelativePaginatedQuery);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorReversed()
	{
		$query = $this->getRelativePaginatedQueryFirstPageReversed();
		$this->assertTrue($query instanceof RelativePaginatedQuery);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithLastPage()
	{
		$query = $this->getRelativePaginatedQueryLastPage();
		$this->assertTrue($query instanceof RelativePaginatedQuery);
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructorWithLastPageReversed()
	{
		$query = $this->getRelativePaginatedQueryLastPageReversed();
		$this->assertTrue($query instanceof RelativePaginatedQuery);
	}
	
	/**
	 * @covers ::query
	 */
	public function testQueryWithFirstPage()
	{
		$expected = new ArrayRecordSet([
			['id' => 1, 'name' => 'name1'],
			['id' => 2, 'name' => 'name2'],
			['id' => 3, 'name' => 'name3'],
			['id' => 4, 'name' => 'name4'],
			['id' => 5, 'name' => 'name5']
		]);

		$query = $this->getRelativePaginatedQueryFirstPage();
		$this->assertEquals($expected, $query->query());
	}

	/**
	 * @covers ::query
	 */
	public function testQueryWithLastPage()
	{
		$expected = new ArrayRecordSet([
			['id' => 17, 'name' => 'name17'],
			['id' => 18, 'name' => 'name18'],
			['id' => 19, 'name' => 'name19']
		]);

		$query = $this->getRelativePaginatedQueryLastPage();
		$this->assertEquals($expected, $query->query());
	}

	/**
	 * @covers ::query
	 */
	public function testQueryWithFirstPageReversed()
	{
		$expected = new ArrayRecordSet([
			['id' => 5, 'name' => 'name5'],
			['id' => 4, 'name' => 'name4'],
			['id' => 3, 'name' => 'name3'],
			['id' => 2, 'name' => 'name2'],
			['id' => 1, 'name' => 'name1']
		]);

		$query = $this->getRelativePaginatedQueryFirstPageReversed();
		$this->assertEquals($expected, $query->query());
	}

	/**
	 * @covers ::query
	 */
	public function testQueryWithLastPageReversed()
	{
		$expected = new ArrayRecordSet([
			['id' => 19, 'name' => 'name19'],
			['id' => 18, 'name' => 'name18'],
			['id' => 17, 'name' => 'name17']
		]);

		$query = $this->getRelativePaginatedQueryLastPageReversed();
		$this->assertEquals($expected, $query->query());
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountWithFirstPage()
	{
		$expected = 15;

		$query = $this->getRelativePaginatedQueryFirstPage();
		$query->query();
		$this->assertEquals($expected, $query->getRemainingCount());
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountWithLastPage()
	{
		$expected = 0;

		$query = $this->getRelativePaginatedQueryLastPage();
		$query->query();
		$this->assertEquals($expected, $query->getRemainingCount());
	}

	/**
	 * @covers ::getRemainingCount
	 */
	public function testGetRemainingCountBeforeQuery()
	{
		$this->expectException(RuntimeException::class);

		$query = $this->getRelativePaginatedQueryLastPage();
		$query->getRemainingCount();
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithFirstPage()
	{
		$query = $this->getRelativePaginatedQueryFirstPage();
		$query->query();
		$this->assertTrue($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithLastPage()
	{
		$query = $this->getRelativePaginatedQueryLastPage();
		$query->query();
		$this->assertFalse($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithFirstPageReversed()
	{
		$query = $this->getRelativePaginatedQueryFirstPageReversed();
		$query->query();
		$this->assertFalse($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordWithLastPageReversed()
	{
		$query = $this->getRelativePaginatedQueryLastPageReversed();
		$query->query();
		$this->assertTrue($query->containsFirstRecord());
	}

	/**
	 * @covers ::containsFirstRecord
	 */
	public function testContainsFirstRecordBeforeQuery()
	{
		$this->expectException(RuntimeException::class);

		$query = $this->getRelativePaginatedQueryFirstPage();
		$query->containsFirstRecord();
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordWithFirstPage()
	{
		$query = $this->getRelativePaginatedQueryFirstPage();
		$query->query();
		$this->assertFalse($query->containsLastRecord());
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordWithLastPage()
	{
		$query = $this->getRelativePaginatedQueryLastPage();
		$query->query();
		$this->assertTrue($query->containsLastRecord());
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordWithFirstPageReversed()
	{
		$query = $this->getRelativePaginatedQueryFirstPageReversed();
		$query->query();
		$this->assertTrue($query->containsLastRecord());
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordWithLastPageReversed()
	{
		$query = $this->getRelativePaginatedQueryLastPageReversed();
		$query->query();
		$this->assertFalse($query->containsLastRecord());
	}

	/**
	 * @covers ::containsLastRecord
	 */
	public function testContainsLastRecordBeforeQuery()
	{
		$this->expectException(RuntimeException::class);

		$query = $this->getRelativePaginatedQueryFirstPage();
		$query->containsLastRecord();
	}
}