mef\Pagination, Version 1.0.1

An implementation of pagination for mefworks/sql. Decorates a Sql select query with
proper parameters to perform pagination on any number of columns.
